//I started with creating the array. I decided to put everything in one array and keep track of all the scores in the array.
var teams = [
	{ name:"Boudewijn", score:0, team: 1, strike: 0},
	{ name:"Vincent", score:0, team: 1, strike: 0},
	{ name:"Piet", score:0, team: 1, strike: 0},
	{ name:"Willem", score:0, team: 1, strike: 0},
	{ name:"Robbert-Jan", score:0, team: 2, strike: 0},
	{ name:"Bartel-Jaap", score:0, team: 2, strike: 0},
	{ name:"Gerrit-Jan", score:0, team: 2, strike: 0},
	{ name:"laurens-Jan", score:0, team: 2, strike: 0},
];

//This for loop keeps track of the rounds. Everytime this loop is completed, 1 round is added. This goes on till all the 10 rounds are done.
for (var round = 1; round < 11; round++) {

	//Here I console log the current round.
	console.log("Round " + round);

	//An enter for neatness.
	console.log("");

	//The code in this for loop makes sure that every player throws twice (or once in case of a strike) each round.
	for (var i = 0; i < teams.length; i++) {

		//Everytime the first player of a team has to throw, his team will be console logged.
		if (i == 0 || i == 4) {
			console.log("Team " +teams[i].team);
			console.log("")
		}

		//The player info gets console logged.
		console.log(teams[i].name + " from team "+teams[i].team+" is up!");

		//The getRandom() function is here executed. See the function at the bottom of the code for an explenation of what happens there.
		getRandom();

		/*if the first random number (the first throw) is 10, there is a special strike message in the console, 10 points are added to the player, 
		the second random number (the second throw) will be set to zero, because there isn't a second throw in case of a strike. And last but not least,
		1 is added to the players strike counter. If the strike counter is 10, the player had a perfect game.*/
		if (randomNumber == 10) {
			console.log("***" +teams[i].name+ " threw a strike!***");
			teams[i].score += randomNumber;
			var randomNumber2 = 0;
			teams[i].strike += 1;
		} 
		  //If the first random number isn't 10, the amount of thrown pins is console logged and the points are added to the players score.
		  else {
			console.log(teams[i].name + " threw "+randomNumber+" pins in his first throw!");
			teams[i].score += randomNumber;

			/*if the points of the first and second throw combined equals to 10, then the player threw a strike. This is also console logged and the points of
			the second throw are given to the player.*/
			if (randomNumber + randomNumber2 == 10) {
				console.log(teams[i].name + " threw "+randomNumber2+" pins in his second throw and made a spare!");
				teams[i].score += randomNumber2;
			} 
			  /*If the points added together aren't equal to then, then the standard message with the player name and points scored this round
			  are console logged and the points of the second throw are given to the player.*/
			  else {
				console.log(teams[i].name + " threw "+randomNumber2+" pins in his second throw!");
				teams[i].score += randomNumber2;
			}
		}

		//Here the total points of this rounds are calculated and placed inside a variable named "roundScore".
		var roundScore =  randomNumber + randomNumber2;

		//After the player has made his throws, his name, the points he scored in the current round and his total score will be logged to the console.
		console.log(teams[i].name+ " knocked over " + roundScore + " pins and now has a total of " +teams[i].score+ " points!");

		console.log("");
	}

	console.log("");

}

//All the player names and their names are console logged with this for loop.
for (var i = 0; i < teams.length; i++) {
	console.log(teams[i].name + " scored a total of " + teams[i].score + " points!");
}

console.log("");

for (var i = 1; i < teams.length; i++ ) {
	document.write(teams[i].name+"<br>");
}

//The array called "winners" is an array with an empty player name and a score.
var winners = [{score:0, name:""}];

/*In this for loop all the scores will be compared to the score of the previous player. Everytime a player has a higher score then the players before him,
his name and score will placed in the array, and it will overwrite the previous player in the array.*/
for (var i = 0; i < teams.length; i++) {
	if(winners[0].score < teams[i].score) {
		var winners = [];
		winners[0] = teams[i];
	} 
	  //If there is another player who has the same highest score, then his name and score will also be added to the array.
	  else if(winners[0].score == teams[i].score) {
		winners.push(teams[i]);
	}
}

//If there is only one players' in the array, his name and score will be alerted in the browser.
if (winners.length == 1) {
	alert(winners[0].name + " won with " + winners[0].score+ " points!");
} 
  //If there is more then one player with the highest score, both their names are placed in the alert.
  else {
	alert(winners[0].name + " and " + winners[1].name+ " both won with " + winners[0].score + " points!");
}

/*This for loop checks if any player has thrown 10 strikes. If this is the case the player will have a perfect game and this is also alerted in a new
alert with his name.*/
for (var i = 0; i < teams.length; i++) {
	if (teams[i].strike == 10) {
		alert(teams[i].name + " had a perfect game!");
	}
}

//In this function, the random numbers are calculated.
function getRandom() {
	//This is the first random number. A random number between 0 and 10 is picked and placed in the "randomNumber" variable and is returned.
	randomNumber = Math.floor((Math.random() * 10) + 1);

	/*To get the second random number, we need to know how many pins are still standing after the first throw. The amount of thrown pins
	of the first throw will be substracted from 10. The result of this is the amount of standing pins.*/
	leftOverPins = 10 - randomNumber;

	//If there are zero pins left, it means that all of the pins are thrown over in the first throw and "0" will be returned.
	if (leftOverPins == 0) {
		randomNumber2 = 0;
	 	return randomNumber2;
	} 
	  //Insted of getting a random number between zero and ten, the second random number will be between 0 and the amount of still standing pins.
	  else {
	 	randomNumber2 = Math.floor((Math.random() * leftOverPins) + 1);
		return randomNumber2;
	}

	return randomNumber;
}