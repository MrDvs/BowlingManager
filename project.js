var team1 = [
	{ name:"Boudewijn", score:0},
	{ name:"Vincent", score:0},
	{ name:"Piet", score:0},
	{ name:"Willem", score:0},
];

var team2 = [
	{ name:"Robbert-jan", score:0},
	{ name:"bartel-jaap", score:0},
	{ name:"gerrit-jan", score:0},
	{ name:"laurens-jan", score:0},
];

for (var round = 1 ; round < 11; round++) {

	console.log("Round ", round);

	console.log("");

	console.log("Team 1:")

	for (var i = 0; i < team1.length; i++) {
		console.log(team1[i].name + " from team 1 is up!");
		getRandom();
		if (randomNumber == 10) {
			console.log(team1[i].name + " threw a strike!");
			team1[i].score += randomNumber;
		} else {
			console.log(team1[i].name + " threw "+randomNumber+" pins in his first throw!");
			team1[i].score += randomNumber;

			getRandom2();
			if (randomNumber + randomNumber2 == 10) {
				console.log(team1[i].name + " threw "+randomNumber2+" pins in his second throw and made a spare!");
				team2[i].score += randomNumber2;
			} else {
				console.log(team1[i].name + " threw "+randomNumber2+" pins in his second throw!");
				team1[i].score += randomNumber2;
			}
		}

		var roundScore =  randomNumber + randomNumber2;

		console.log(team2[i].name+ " knocked over " + roundScore + " pins and now has a total of " +team2[i].score+ " points!")
		console.log("");
	}

	console.log("");

	console.log("Team 2:")

	for (var i = 0; i < team2.length; i++) {
		console.log(team2[i].name + " from team 2 is up!");
		getRandom();
		if (randomNumber == 10) {
			console.log(team2[i].name + " threw a strike!");
			team2[i].score += randomNumber;
			var randomNumber2 = 0;
		} else {
			console.log(team2[i].name + " threw "+randomNumber+" pins in his first throw!");
			team2[i].score += randomNumber;

			getRandom2();
			if (randomNumber + randomNumber2 == 10) {
				console.log(team2[i].name + " threw "+randomNumber2+" pins in his second throw and made a spare!");
				team2[i].score += randomNumber2;
			} else {
				console.log(team2[i].name + " threw "+randomNumber2+" pins in his second throw!");
				team2[i].score += randomNumber2;
			}
		}

		var roundScore =  randomNumber + randomNumber2;

		console.log(team2[i].name+ " knocked over " + roundScore + " pins and now has a total of " +team2[i].score+ " points!")
		console.log("");
	}

		console.log("");

}

for (var i = 0; i < team1.length; i++) {
	console.log(team1[i].name + " scored a total of " + team1[i].score + " points!");
}

console.log("");

for (var i = 0; i < team2.length; i++) {
	console.log(team2[i].name + " scored a total of " + team2[i].score + " points!");
}

alert();

function getRandom() {
	randomNumber = Math.floor((Math.random() * 10) + 1);
	return randomNumber;
}

function getRandom2() {
	var test = 10 - randomNumber;

	if (test == 0) {
		randomNumber2 = 0;
		return randomNumber2;
	} else {
		randomNumber2 = Math.floor((Math.random() * test) + 1);
		return randomNumber2;
	}
}

var table = document.getElementById("table");